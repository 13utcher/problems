package common;

/**
 * Created by Evgeni on 12/14/14.
 */
public interface First10TasksResolver {
    public void task1();
    public void task2();
    public void task3();
    public void task4();
    public void task5();
    public void task6();
    public void task7();
    public void task8();
    public void task9();
    public void task10();
}
