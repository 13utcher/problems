package common;



import evgeni.EvgeniTasksResovler;
import igrzxc.IgrzxcTasksResovler;
import pasha.PashaTasksResovler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ProblemsTaskExecutor {

    public static int POINTS_FOR_1_TASK = 100;
    public static int POINTS_FOR_2_TASK = 100;
    public static int POINTS_FOR_3_TASK = 100;
    public static int POINTS_FOR_4_TASK = 100;
    public static int POINTS_FOR_5_TASK = 100;
    public static int POINTS_FOR_6_TASK = 100;
    public static int POINTS_FOR_7_TASK = 100;
    public static int POINTS_FOR_8_TASK = 100;
    public static int POINTS_FOR_9_TASK = 100;
    public static int POINTS_FOR_10_TASK = 100;




    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<First10TasksResolver> resolvers = new ArrayList<First10TasksResolver>();
        resolvers.add(new EvgeniTasksResovler());
        resolvers.add(new IgrzxcTasksResovler());
        resolvers.add(new PashaTasksResovler());

        ArrayList<TaskPerformanceSaver> taskPerformanceSavers = new ArrayList<TaskPerformanceSaver>();

        long startTime;
        BigDecimal averageSpentTime;
        for (First10TasksResolver first10TasksResolver: resolvers){
            String resolverName = first10TasksResolver.getClass().getSimpleName();
            for(int taskNumber = 1; taskNumber <=10; taskNumber++){
                Method method = first10TasksResolver.getClass().getMethod("task" + taskNumber);
                startTime = System.currentTimeMillis();
                for (int i = 0; i < 100; i++){
                    method.invoke(first10TasksResolver);
                }
                averageSpentTime = new BigDecimal((System.currentTimeMillis() - startTime)).divide(new BigDecimal(100),2, RoundingMode.HALF_UP);
                taskPerformanceSavers.add(new TaskPerformanceSaver("task" + taskNumber, resolverName, averageSpentTime));
            }
        }
        BigDecimal evgeniPoints = new BigDecimal(0);
        BigDecimal igrzxcPoints = new BigDecimal(0);
        BigDecimal pavelPoints = new BigDecimal(0);

        for(int taskNumber = 1; taskNumber <=10; taskNumber++){
            BigDecimal  evgeniMethodDuration = new BigDecimal(Long.MAX_VALUE);
            BigDecimal  igrzxcMethodDuration = new BigDecimal(Long.MAX_VALUE);
            BigDecimal  pavelMethodDuration = new BigDecimal(Long.MAX_VALUE);

            String bestPerformer ="";
            BigDecimal bestPerformance = new BigDecimal(0);
            BigDecimal minimalDuration = new BigDecimal(Long.MAX_VALUE);

            for(TaskPerformanceSaver taskPerformanceSaver : taskPerformanceSavers){
                if(taskPerformanceSaver.getTask().equals("task" + taskNumber)){
                    if(taskPerformanceSaver.getName().equals("EvgeniTasksResovler")){
                        evgeniMethodDuration = taskPerformanceSaver.getAverageTimeInMilliseconds();
                    }
                    if(taskPerformanceSaver.getName().equals("IgrzxcTasksResovler")){
                        igrzxcMethodDuration = taskPerformanceSaver.getAverageTimeInMilliseconds();
                    }
                    if(taskPerformanceSaver.getName().equals("PashaTasksResovler")){
                        pavelMethodDuration = taskPerformanceSaver.getAverageTimeInMilliseconds();
                    }
                    if((new BigDecimal(1).divide(taskPerformanceSaver.getAverageTimeInMilliseconds(),2, RoundingMode.HALF_UP)).compareTo(bestPerformance)>0){
                        minimalDuration = taskPerformanceSaver.getAverageTimeInMilliseconds();
                        bestPerformance = new BigDecimal(1).divide(minimalDuration,2, RoundingMode.HALF_UP);
                        bestPerformer = taskPerformanceSaver.getName();
                    }
                }
            }

            System.out.println("evgeni method execution duration of task " + taskNumber + " = " + evgeniMethodDuration + " milliseconds");
            System.out.println("igrzxc method execution duration of task " + taskNumber + " = " + igrzxcMethodDuration + " milliseconds");
            System.out.println("pavel method execution duration of task " + taskNumber + " = " + pavelMethodDuration + " milliseconds");
            System.out.println("fastest decision of task " + taskNumber + " of class " + bestPerformer + " with time = " + minimalDuration);

            BigDecimal totalPerformance = (new BigDecimal(1).divide(evgeniMethodDuration,2, RoundingMode.HALF_UP).add(new BigDecimal(1).divide(igrzxcMethodDuration,2, RoundingMode.HALF_UP)).add( new BigDecimal(1).divide(pavelMethodDuration,2, RoundingMode.HALF_UP)));

            evgeniPoints =evgeniPoints.add( (new BigDecimal(1).divide(evgeniMethodDuration,2, RoundingMode.HALF_UP)).divide(totalPerformance,2, RoundingMode.HALF_UP).multiply(new BigDecimal(100))) ;//change to constant
            igrzxcPoints =igrzxcPoints.add( (new BigDecimal(1).divide(igrzxcMethodDuration,2, RoundingMode.HALF_UP)).divide(totalPerformance,2, RoundingMode.HALF_UP).multiply(new BigDecimal(100))) ;//change to constant
            pavelPoints =pavelPoints.add( (new BigDecimal(1).divide(pavelMethodDuration,2, RoundingMode.HALF_UP)).divide(totalPerformance,2, RoundingMode.HALF_UP).multiply(new BigDecimal(100))) ;//change to constant
        }

        String winnerName="";
        BigDecimal maxPointsOfWinner;
        if (evgeniPoints.compareTo( igrzxcPoints) > 0 && evgeniPoints.compareTo(pavelPoints) > 0){
            winnerName = "Evgeni";
            maxPointsOfWinner = evgeniPoints;
        } else if(igrzxcPoints.compareTo(pavelPoints) > 0){
            winnerName = "Igr";
            maxPointsOfWinner = igrzxcPoints;
        }else{
            winnerName = "Pavel";
            maxPointsOfWinner = pavelPoints;
        }


        System.out.println("Winner with maximum points = " + maxPointsOfWinner + " is " + winnerName);

    }
}
