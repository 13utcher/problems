package common;

import java.math.BigDecimal;

/**
 * Created by Evgeni on 12/14/14.
 */
public class TaskPerformanceSaver {

    private String task;

    private String name;

    private BigDecimal averageTimeInMilliseconds;

    public TaskPerformanceSaver(String task, String name, BigDecimal averageTimeInMilliseconds) {
        this.task = task;
        this.name = name;
        this.averageTimeInMilliseconds = averageTimeInMilliseconds;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAverageTimeInMilliseconds() {
        return averageTimeInMilliseconds;
    }

    public void setAverageTimeInMilliseconds(BigDecimal averageTimeInMilliseconds) {
        this.averageTimeInMilliseconds = averageTimeInMilliseconds;
    }
}
