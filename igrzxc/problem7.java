package projecteuler;

public class problem7 {

    public static void main(String[] args) {

        int primeCount = 6;
        for (int i = 14; i > 0; i++) {
            for (int k = 2; k <= i; k++) {
                if (i % k == 0) {
                    if (k == i) {
                        primeCount++;
                    }
                    k = 0;
                    break;
                }
                }
            if (primeCount == 10001) {
                System.out.println(i);
                return;
            }
        }
    }
}

