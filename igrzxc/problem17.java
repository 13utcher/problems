package projecteuler;

import java.util.HashMap;

public class problem17 {

    public static void main(String[] args) {

        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        hashMap.put(1, "one");
        hashMap.put(2, "two");
        hashMap.put(3, "three");
        hashMap.put(4, "four");
        hashMap.put(5, "five");
        hashMap.put(6, "six");
        hashMap.put(7, "seven");
        hashMap.put(8, "eight");
        hashMap.put(9, "nine");
        hashMap.put(10, "ten");
        hashMap.put(11, "eleven");
        hashMap.put(12, "twelve");
        hashMap.put(13, "thirteen");
        hashMap.put(14, "fourteen");
        hashMap.put(15, "fifteen");
        hashMap.put(16, "sixteen");
        hashMap.put(17, "seventeen");
        hashMap.put(18, "eighteen");
        hashMap.put(19, "nineteen");
        hashMap.put(20, "twenty");
        hashMap.put(30, "thirty");
        hashMap.put(40, "forty");
        hashMap.put(50, "fifty");
        hashMap.put(60, "sixty");
        hashMap.put(70, "seventy");
        hashMap.put(80, "eighty");
        hashMap.put(90, "ninety");
        hashMap.put(100, "hundred");
        hashMap.put(1000, "onethousand");

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 1; i <= 1000; i++) {
            if (i <= 20 || i == 30 || i == 40 || i == 50|| i == 60 || i == 70 || i == 80 || i == 90 ) {
                stringBuilder.append(hashMap.get(i));
            }
            if (i > 20 && i < 100 && i%10 != 0) {
                stringBuilder.append(hashMap.get(i - i%10) + hashMap.get(i%10));
            }
            if (i > 120 && i < 1000 && i%100%10 != 0 && i%100 > 20) {
                stringBuilder.append(hashMap.get((i - i%100)/100) + hashMap.get(100) + "and" + hashMap.get(i%100- i%100%10) + hashMap.get(i%100%10));
            }
            if (i >= 110 && i%100%10 == 0 && i%100 != 0) {
                stringBuilder.append(hashMap.get((i - i%100)/100) + hashMap.get(100) + "and" + hashMap.get(i%100));
            }
            if (i%100 == 0 && i != 1000) {
                stringBuilder.append(hashMap.get(i/100) + hashMap.get(100));
            }
            if (i == 1000) {
                stringBuilder.append(hashMap.get(1000));
            }
            if (i > 100 && i%100 < 20 && i%100 != 0 && i%100 != 10) {
                stringBuilder.append(hashMap.get((i - i%100)/100) + hashMap.get(100) + "and" + hashMap.get(i%100));
            }


        }
        System.out.println(stringBuilder);
        System.out.println(stringBuilder.length());




    }
}
