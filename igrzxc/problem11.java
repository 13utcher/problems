package projecteuler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class problem11 {

    public static void main(String[] args) throws IOException {

        String fileName = "file.txt";
        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String s = "";
        StringBuilder stringBuilder = new StringBuilder();
        while ((s = bufferedReader.readLine()) != null) {
            stringBuilder.append(s + " ");
        }
        System.out.println(stringBuilder);
        int a[][] = new int[20][20];
        // после того как выше из файла все преобразовано было в строку мы заполняем двумерный массив
        for (int i = 0; i < 20; i++) {
            for (int k = 0; k < 20; k++) {
                //индекс с которого парсится каждое отдельное число
                int firstIndx = ((20 * i) + k) * 3;
                a[i][k] = Integer.parseInt(stringBuilder.substring(firstIndx, firstIndx + 2));
            }
        }
        long max = 0;
        long hor = 0;
        long vert = 0;
        long diag1 = 0;
        long diag2 = 0;
        // считаютяс наибольшие произведения по всем направлениям
        for (int i = 0; i < 17; i++) {
            for (int k = 0; k < 17; k++) {
                hor = a[i][k] * a[i][k + 1] * a[i][k + 2] * a[i][k + 3];
                vert = a[i][k] * a[i + 1][k] * a[i + 2][k] * a[i + 3][k];
                diag1 = a[i][k] * a[i + 1][k + 1] * a[i + 2][k + 2] * a[i + 3][k + 3];
                diag2 = a[i][19 - k] * a[i + 1][18 - k] * a[i + 2][17 - k] * a[i + 3][16 - k];
                if (hor > max) {
                    max = hor;
                }
                if (vert > max) {
                    max = vert;
                }
                if (diag1 > max) {
                    max = diag1;
                }
                if (diag2 > max) {
                    max = diag2;
                }

            }
        }
        System.out.println(max);
    }
}
