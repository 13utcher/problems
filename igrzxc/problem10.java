package projecteuler;

public class problem10 {

    public static void main(String[] args) {

        long sum = 10;
        double c = 0;


        // без введения переменной "с" программа работает очень медленно, когда вводишь "с" проверка именно простых чисел происходит в разы быстрее
        // т.к. "с" ограничивает кол-во проверяемых делителей
        for (int i = 6; i < 2000000; i++) {
            c = (double)i/2;
            for (int k = 2; k <= c + 1; k++) {
                if (i % k != 0) {
                    c = (double)i/k;
                    if (k >= c) {
                        sum = sum + i;

                    }
                } else {
                    break;

            }
        }
    }
        System.out.println(sum);
}

}
