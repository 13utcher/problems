package projecteuler;

import java.util.HashMap;

public class problem19 {

    public static void main(String[] args) {


        HashMap<Integer, Integer> maxDayMap = new HashMap<Integer, Integer>();
        int maxDay = 0;
        maxDayMap.put(1, 31);
        maxDayMap.put(2, 28);
        maxDayMap.put(3, 31);
        maxDayMap.put(4, 30);
        maxDayMap.put(5, 31);
        maxDayMap.put(6, 30);
        maxDayMap.put(7, 31);
        maxDayMap.put(8, 31);
        maxDayMap.put(9, 31);
        maxDayMap.put(10, 31);
        maxDayMap.put(11, 30);
        maxDayMap.put(12, 31);
        int allDaysCount = 0;
        int sundayCount = 0;
        for (int year = 1900; year < 2001; year++) {
            for (int month = 1; month <= 12; month++) {
                maxDay = maxDayMap.get(month);
                if (month == 2 && (year % 4 == 0 || ((year == 1900 || year == 2000) && year % 400 == 0))) {
                    maxDay = maxDayMap.get(month) + 1;
                }

                for (int day = 1; day < maxDay; day++) {
                    allDaysCount++;
                    if (allDaysCount % 7 == 0 && day == 1) {
                        sundayCount++;
                    }
                }
            }
        }
        System.out.println(sundayCount);
    }
}
