package projecteuler;

import java.util.ArrayList;

public class problem2 {

    public static void main(String[] args) {
        int curFib = 0;
        ArrayList<Integer> listFib = new ArrayList<Integer>();
        listFib.add(1);
        listFib.add(2);

        for (int i = 2; curFib < 4000000; i++ ) {
            curFib = listFib.get(i - 2) + listFib.get(i - 1);
            listFib.add(curFib);
        }
        Integer summn = 0;
        for (Integer fib : listFib) {
            if (fib % 2 == 0) {
                summn += fib;
            }
        }
        System.out.println(listFib);
        System.out.println(summn);
    }
}
