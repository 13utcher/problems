package projecteuler;

import java.util.HashMap;
import java.util.HashSet;

public class problem21 {

    public static void main(String[] args) {

        HashMap<Integer, Integer> numberAndSumOfProperDivisors = new HashMap<Integer, Integer>();
        numberAndSumOfProperDivisors.put(0, 0);
        numberAndSumOfProperDivisors.put(1, 0);
        int sumOfProperDivisors = 1;
        for (int i = 2; i < 10000; i++) {
            for (int k = 2; k < i; k++) {
                if (i % k == 0) {
                    sumOfProperDivisors += k;
                }
            }
            numberAndSumOfProperDivisors.put(i, sumOfProperDivisors);
            sumOfProperDivisors = 1;
        }
        int sum = 0;
        HashSet<Integer> hashSet = new HashSet<Integer>(numberAndSumOfProperDivisors.values());
        for (int sumOfDivisors: hashSet) {
            if (sumOfDivisors < 10000 && numberAndSumOfProperDivisors.get(numberAndSumOfProperDivisors.get(sumOfDivisors)) != null) {
                if (numberAndSumOfProperDivisors.get(numberAndSumOfProperDivisors.get(sumOfDivisors)) == sumOfDivisors && numberAndSumOfProperDivisors.get(sumOfDivisors) != sumOfDivisors) {
                    System.out.println(sumOfDivisors);
                    sum += sumOfDivisors;
                }
            }
        }

        System.out.println(sum);
    }


}
