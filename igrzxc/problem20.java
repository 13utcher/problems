package projecteuler;

import java.math.BigInteger;

public class problem20 {

    public static void main(String[] args) {

        BigInteger fact = BigInteger.valueOf(100);
        for (int i = 99; i > 0; i--) {
            fact = fact.multiply(BigInteger.valueOf(i));
        }

        String s = String.valueOf(fact);
        int sumDigit = 0;
        for (int i = 0; i < s.length(); i++) {
            sumDigit += Integer.parseInt(s.substring(i, i + 1));
        }



        System.out.println(sumDigit);

    }
}
