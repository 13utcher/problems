package projecteuler;

public class problem12 {

    public static void main(String[] args) {

        int divisorsCount = 2;
        double c = 0;
        long value = 1;
        for (long valueOthr = 2; valueOthr < 100000000000000l; valueOthr++) {
            value = value + valueOthr;
            System.out.println(value);
            c = (double) value / 2;
            for (int i = 2; i <= c + 1 && c + divisorsCount > 500 ; i++) {
                if (value % i == 0) {
                    divisorsCount += 2;

                    if (divisorsCount > 500) {
                        System.out.println(value);
                        return;
                    }
                } else {
                    c = (double) value / i;
                }
            }
            divisorsCount = 2;
        }
    }
}
