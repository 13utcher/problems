package projecteuler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

public class problem22 {

    public static void main(String[] args) throws IOException {

        FileReader fileReader = new FileReader("names.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String s = bufferedReader.readLine();
        String[] sArr  = s.replace("\"", "").split(",");
        TreeMap<String,Integer> treeMap = new TreeMap<String, Integer>();
        TreeMap<String,Integer> treeMap2 = new TreeMap<String, Integer>();
        TreeSet<String> treeSet = new TreeSet<String>();
        for (String name: sArr) {
            treeSet.add(name);
        }
        int i = 1;
        int score = 0;
        System.out.println(treeSet);
        TreeMap<Character, Integer> alphabet = new TreeMap<Character, Integer>();
        int f = 1;
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            alphabet.put(ch, f);
            f++;
        }
        int sunScore = 0;
        for (String name: treeSet) {
            for (int k = 0; k < name.length(); k++) {
                score += alphabet.get(name.charAt(k));
            }
            treeMap2.put(name, score*i);
            sunScore += score*i;
            score = 0;
            i++;

        }
        System.out.println(alphabet);
        System.out.println(treeMap2);
        System.out.println(sunScore);








    }
}
