package projecteuler;

public class problem3 {

    public static void main(String[] args) {

        for ( long k = 3 ; k > 0; k++) {
            long g = 600851475143L/k;
            if (600851475143L % g == 0) {
                long i = g/2;
                while (g % i != 0) {
                    i--;
                    if (i == 1) {
                        System.out.println(g);
                        return;
                    }
                }
            }
        }
    }
}
