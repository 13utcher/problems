package projecteuler;

import java.util.HashMap;
import java.util.Map;

public class problem14 {

    public static void main(String[] args) {

        HashMap<Long, Integer> termsAndLongOfChain = new HashMap<Long, Integer>();
        long currentNumber = 0;
        int termsCount = 0;
        for (long startNumber = 2; startNumber < 1000000; startNumber++) {
            currentNumber = startNumber;
            while (currentNumber != 1) {
                if (termsAndLongOfChain.containsKey(currentNumber)) {
                    termsCount = termsCount + termsAndLongOfChain.get(currentNumber);
                    break;
                }
                if (currentNumber % 2 == 0) {
                    currentNumber = currentNumber/2;
                    termsCount++;
                } else {
                    currentNumber = currentNumber*3 + 1;
                    termsCount++;
                }
            }
            termsAndLongOfChain.put(startNumber, termsCount);
            termsCount = 0;
        }
        int maxChain = 0;
        Long numberOfMaxChain = 0L;
        for (Map.Entry<Long, Integer> entry: termsAndLongOfChain.entrySet()) {
            if ((int)entry.getValue() > maxChain) {
                maxChain = (int)entry.getValue();
                numberOfMaxChain = entry.getKey();
            }

        }
        System.out.println(numberOfMaxChain);
    }
}
