package projecteuler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

public class problem13 {

    public static void main(String[] args) throws IOException {

        FileReader fileReader = new FileReader("file2.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String s;
        BigInteger f = BigInteger.valueOf(0);
        while ((s = bufferedReader.readLine()) != null) {
            BigInteger bigInteger = new BigInteger(s);
            f = bigInteger.add(f);
        }
        System.out.println(f.toString().substring(0, 10));
    }
}
