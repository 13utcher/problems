package igrzxc;
import common.First10TasksResolver;

public class IgrzxcTasksResovler implements First10TasksResolver {


    @Override
    public void task1() {
        int sum = 0;
        for (int i = 1; i < 1000; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }

    @Override
    public void task2() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task3() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task4() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task5() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task6() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task7() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task8() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task9() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task10() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }
}
