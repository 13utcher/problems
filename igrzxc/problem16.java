package projecteuler;

import java.math.BigInteger;

public class problem16 {

    public static void main(String[] args) {

        BigInteger s = BigInteger.valueOf(1);
        for (int i = 1; i <= 1000; i++) {
            s = s.multiply(BigInteger.valueOf(2));
        }
        int k = 0;
        int sum = 0;
        String s1 = s + "q";
        while (!s1.substring(k, k + 1).equals("q")) {
            sum += Integer.parseInt(s.toString().substring(k, k + 1));
            k++;
        }
        System.out.println(sum);
    }
}
