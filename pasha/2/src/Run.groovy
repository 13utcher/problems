def list = [1, 2]
def sum = 2
def newEntry
def startTime = System.nanoTime()
while ((newEntry = list.last() + list.get(list.size() - 2)) <= 4000000) {
    list << newEntry
    if (newEntry % 2 == 0) {
        sum += newEntry
    }
}
def endTime = System.nanoTime()
println "sum = ${sum}"
print "time= ${(endTime - startTime) / 1000000000} seconds"