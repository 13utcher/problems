def list = []
def startTime = System.nanoTime()
1000.times {
    if ((it % 3 == 0) || (it % 5 == 0) || (it % 15 == 0)) {
        list << it
    }
}
def endTime = System.nanoTime()
println(list.sum())
println("time= ${(endTime - startTime) / 1000000000} seconds")