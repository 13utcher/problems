import java.util.ArrayList;
import java.util.List;

/*
* A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
  Find the largest palindrome made from the product of two 3-digit numbers.
*/
public class Run {
    public static void main(String[]args){
        int maxThreeDigit = 999;
        int maxMultiply = maxThreeDigit * maxThreeDigit;
        int thatsTheNumber = 0;
        long startTime = System.nanoTime();
        outer:
        for (int i = maxMultiply; i >= 10000; i--) {
            if (isPalindrome(i)) {
                for (int j = maxThreeDigit; j >= 100; j--) {
                    if ((i % j == 0) && (i / j / 100 < 10)) {
                        thatsTheNumber = i;
                        System.out.println("first = " + j);
                        System.out.println("second = " + i/j);
                        break outer;
                    }
                }
            }
        }
        long endTime = System.nanoTime();
        System.out.println("time = " + (endTime - startTime) / 1000000000 + " seconds");
        System.out.println("result = " + thatsTheNumber);
    }

    public static boolean isPalindrome(int number) {
        boolean result = false;
        List<Integer> numbers = new ArrayList<Integer>();
        while (number > 0) {
            int newNumber = number - (number / 10) * 10;
            numbers.add(newNumber);
            number = number / 10;
        }
        for (int i = 0; i <= numbers.size() / 2; i++) {
            if (numbers.get(i) == numbers.get(numbers.size() - i - 1)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }
}
