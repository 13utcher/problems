public class Run {
    public static void main(String[]args){
        long number = 600851475143L;
        long maxFactor = 0;
        long startTime = System.nanoTime();
        for (long i = 2; i <= number; i++) {
            if (number % i == 0) {
                maxFactor = (number / i);
                break;
            }
        }
        long max = 0;
        outer:
        for (long i = maxFactor; i >=3 ; i--) {
            if (number % i == 0) {
                int k = 0;
                for (long j = 1; j <= i ; j++) {
                    if (i % j == 0) {
                        k++;
                        if (k > 2) {
                            break;
                        }
                    }
                    if ((j == i) && (k == 2)) {
                        max = i;
                        break outer;
                    }
                }
            }
        }
        long endTime = System.nanoTime();
        System.out.println("time = " + (endTime - startTime) / 1000000000 + " seconds");
        System.out.println(max);
    }
}
