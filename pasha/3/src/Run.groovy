/*
* The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
* */

def number = 600851475143
def maxFactor
def startTime = System.nanoTime()
for (def i = 2; i <= number; i++) {
    if (number % i == 0) {
        maxFactor = number / i
        println("maxFactor = ${maxFactor}")
        break
    }
}
def max = 0
outer:
for (long i = maxFactor; i >=3 ; i--) {
    if (number % i == 0) {
        int k = 0
        for (long j = 1; j <= i ; j++) {
            if (i % j == 0) {
                k++
                if (k > 2) {
                    break
                }
            }
            if ((j == i) && (k == 2)) {
                max = i
                break outer
            }
        }
    }
}
def endTime = System.nanoTime()
println("time = ${(endTime - startTime) / 1000000000} seconds")
println("max = ${max}")