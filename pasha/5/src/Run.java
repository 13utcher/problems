package src;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/*2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 *What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
public class Run {
    public static void main(String[] args) {
    	long startTime = System.nanoTime();
    	int number = 20;
    	int thatsTheNumber = 0;
    	boolean isNumberFound = false;
    	while (!isNumberFound) {
    		int numOfDivisionIndex = 0;
    		for (int i = 10; i <= 20; i++) {
    			if (number % i == 0) {
    				numOfDivisionIndex++;
    			} else {
    				break;
    			}
    		}
    		if (numOfDivisionIndex == 11) {
    			thatsTheNumber = number;
    			isNumberFound = true;
    		}
    		number++;
    	}
    	long endTime = System.nanoTime();
    	NumberFormat formatter = new DecimalFormat("#0.00");     
        System.out.println("time = " + formatter.format((double)(endTime - startTime) / 1000000000) + " seconds");
        System.out.println("result = " + thatsTheNumber);
    }
}