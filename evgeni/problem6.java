package evgeni;

import java.util.ArrayList;

public class problem6 {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        long AMOUNT_OF_NATURAL_NUMBERS = 100;
        long sumOfSquare = (long) ((double)1 / 3 * Math.pow(AMOUNT_OF_NATURAL_NUMBERS, 3) + (double)1 / 2 * Math.pow(AMOUNT_OF_NATURAL_NUMBERS, 2) + (double)1 / 6 * Math.pow(AMOUNT_OF_NATURAL_NUMBERS, 1));
        long sumOfNumbers = ((1 + AMOUNT_OF_NATURAL_NUMBERS)  * AMOUNT_OF_NATURAL_NUMBERS/ 2);
        System.out.println((long)Math.pow(sumOfNumbers, 2) - sumOfSquare);
        System.out.println(System.nanoTime() - startTime);
    }

}

