package evgeni;

import java.util.ArrayList;

public class problem5 {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        int smallestDivisibleNumber = 2;
        ArrayList<Integer> compoundNumbers = new ArrayList<Integer>();
        compoundNumbers.add(2);
        outer:
        for (int i = 2; i <= 20; i++) {
            int smallestPartialNumber = i;
            for (int compoundNumber : compoundNumbers) {
                if (smallestPartialNumber % compoundNumber == 0) {
                    smallestPartialNumber = smallestPartialNumber / compoundNumber;
                    if (smallestPartialNumber == 1) continue outer;
                }
            }
            compoundNumbers.add(smallestPartialNumber);
            smallestDivisibleNumber = smallestDivisibleNumber * smallestPartialNumber;
        }
        System.out.println(smallestDivisibleNumber);
        System.out.println(System.currentTimeMillis() - startTime);
    }

}

