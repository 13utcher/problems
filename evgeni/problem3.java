package evgeni;

import java.util.ArrayList;
import java.util.Arrays;

public class problem3 {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        long givenNumber =  600851475143L;
        boolean[] primesLowerSqrt = new boolean[(int) Math.sqrt(givenNumber)+2];

        Arrays.fill(primesLowerSqrt,true);
        primesLowerSqrt[0]=primesLowerSqrt[1]=false;
        for (int i=2;i<primesLowerSqrt.length;i++) {
            if(primesLowerSqrt[i]) {
                for (int j=2;i*j<primesLowerSqrt.length;j++) {
                    primesLowerSqrt[i*j]=false;
                }
            }
        }

        ArrayList<Integer> primesLowerSqrtAsNumbers = new ArrayList<Integer>();
        for(int i=2;i<primesLowerSqrt.length;i++){
            if(primesLowerSqrt[i]){
                primesLowerSqrtAsNumbers.add(i);
            }
        }

        for(int i = primesLowerSqrtAsNumbers.size()-1; i >=0; i--){
            if(givenNumber % primesLowerSqrtAsNumbers.get(i)==0){
                System.out.println(primesLowerSqrtAsNumbers.get(i));
                break;
            }
        }
        System.out.println(System.currentTimeMillis() - startTime);

        //right answer:
       /* long startTime = System.currentTimeMillis();
        int i=2;
        long  x=600851475143l;
        while(i<x){
            if (x%i==0){
                x=x/i;
                continue;
            }
            i=i+1;
        }
        System.out.println(x);
        System.out.println(System.currentTimeMillis() - startTime);*/
    }

}
