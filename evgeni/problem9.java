package evgeni;

public class problem9 {
    private static final int numberOfAdjacentDigits = 13;

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        int a, b, c;
        double sqrtOfDescriminant;
        for (int p = 332; p >= 1; p--) {
            //дискриминант уравнения, в котором a нахоидтся через p, где p - разница между a и b
            sqrtOfDescriminant = Math.sqrt((4000 - 2 * p) * (4000 - 2 * p) + 8 * (2000 * p - 1000000));
            //дискриминант должен быть натуральным числом. Если данная проверка возвращает true, то задача решена. Остальные решения - неверны.
            if (sqrtOfDescriminant == (int) sqrtOfDescriminant) {
                a = (-4000 + 2 * p + (int) sqrtOfDescriminant) / -4;
                b = a + p;
                c = 1000 - a - b;
                System.out.println(a * b * c);
                System.out.println(System.currentTimeMillis() - startTime);
                return;
            }
        }
    }

}

