package evgeni;

import java.util.Arrays;

public class problem7 {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        int notPrimesOnOnePrimeOn10000DiapasonWithReserve = 10+1;
        boolean[] primes = new boolean[notPrimesOnOnePrimeOn10000DiapasonWithReserve * 10001];
        Arrays.fill(primes, true);
        primes[0]=primes[1]=false;
        int primesCounter = 0;
        for (int i=2;i<primes.length;i++) {
            if(primes[i]) {
                if(++primesCounter == 10001){
                    System.out.println(i);
                    System.out.println(System.currentTimeMillis() - startTime);
                    break;
                }
                for (int j=2;i*j<primes.length;j++) {
                    primes[i*j]=false;
                }
            }
        }

    }

}

