package evgeni;

/**
 * Created by Evgeni on 12/20/14.
 */
public class problem1 {

    public static void main(String[] args) {
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3 = 999/3;
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5 = 999/5;
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15 = 999/15;
        int FIRST_DIVISIBLE_ON_3_NUMBER = 3;
        int FIRST_DIVISIBLE_ON_5_NUMBER = 5;
        int FIRST_DIVISIBLE_ON_15_NUMBER = 15;
        //используем формулу арифметической прогрессии
        int sumOf3DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_3_NUMBER +3*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3/2;
        int sumOf5DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_5_NUMBER +5*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5/2;
        int sumOf15DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_15_NUMBER +15*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15/2;
        int totalSum = sumOf3DivisibleNumbers + sumOf5DivisibleNumbers - sumOf15DivisibleNumbers;
        System.out.println(totalSum);
    }
}
