package evgeni;

import java.util.Arrays;

public class problem10 {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        boolean[] primes = new boolean[2000000];
        Arrays.fill(primes, true);
        primes[0]=primes[1]=false;
        long primesSum = 0;
        for (int i=2;i<primes.length;i++) {
            if(primes[i]) {
                primesSum+=i;
                for (int j=2;i*j<primes.length;j++) {
                    primes[i*j]=false;
                }
            }
        }
        System.out.println(primesSum);
        System.out.println(System.currentTimeMillis() - startTime);

    }

}

