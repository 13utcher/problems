package evgeni;
import common.First10TasksResolver;

import java.util.ArrayList;
import java.util.Arrays;

public class EvgeniTasksResovler implements First10TasksResolver {


    @Override
    public void task1() {
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3 = 999/3;
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5 = 999/5;
        int AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15 = 999/15;
        int FIRST_DIVISIBLE_ON_3_NUMBER = 3;
        int FIRST_DIVISIBLE_ON_5_NUMBER = 5;
        int FIRST_DIVISIBLE_ON_15_NUMBER = 15;
        int sumOf3DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_3_NUMBER +3*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_3/2;
        int sumOf5DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_5_NUMBER +5*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_5/2;
        int sumOf15DivisibleNumbers = (2*FIRST_DIVISIBLE_ON_15_NUMBER +15*(AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15-1))*AMOUNT_OF_NUMBERS_DIVISIBLE_ON_15/2;
        int totalSum = sumOf3DivisibleNumbers + sumOf5DivisibleNumbers - sumOf15DivisibleNumbers;
        System.out.println(totalSum);
    }

    @Override
    public void task2() {
        int first = 1;
        int second = 2;
        int next;
        int sum=0;
        while (second < 4000000) {
            if(second%2==0){
                sum+=second;
            }
            next = first + second;
            first = second;
            second = next;
        }
        System.out.println(sum);
    }

    @Override
    public void task3() {
        long startTime = System.currentTimeMillis();
        long givenNumber =  600851475143L;
        boolean[] primesLowerSqrt = new boolean[(int) Math.sqrt(givenNumber)+2];

        Arrays.fill(primesLowerSqrt, true);
        primesLowerSqrt[0]=primesLowerSqrt[1]=false;
        for (int i=2;i<primesLowerSqrt.length;i++) {
            if(primesLowerSqrt[i]) {
                for (int j=2;i*j<primesLowerSqrt.length;j++) {
                    primesLowerSqrt[i*j]=false;
                }
            }
        }

        ArrayList<Integer> primesLowerSqrtAsNumbers = new ArrayList<Integer>();
        for(int i=2;i<primesLowerSqrt.length;i++){
            if(primesLowerSqrt[i]){
                primesLowerSqrtAsNumbers.add(i);
            }
        }

        for(int i = primesLowerSqrtAsNumbers.size()-1; i >=0; i--){
            if(givenNumber % primesLowerSqrtAsNumbers.get(i)==0){
                System.out.println(primesLowerSqrtAsNumbers.get(i));
                break;
            }
        }
        System.out.println(System.currentTimeMillis() - startTime);
    }

    @Override
    public void task4() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task5() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task6() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task7() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task8() {

    }

    @Override
    public void task9() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }

    @Override
    public void task10() {
        System.out.println("remove this string when implement this method. This system out need for avoid 0 duration of the method - division by zero exception");
    }
}
